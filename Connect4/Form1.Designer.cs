﻿namespace Connect4
{
	partial class Form1
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
			this.pB_R5C0 = new System.Windows.Forms.PictureBox();
			this.pB_R5C1 = new System.Windows.Forms.PictureBox();
			this.pB_R5C2 = new System.Windows.Forms.PictureBox();
			this.pB_R5C3 = new System.Windows.Forms.PictureBox();
			this.pB_R5C4 = new System.Windows.Forms.PictureBox();
			this.pB_R5C5 = new System.Windows.Forms.PictureBox();
			this.pB_R5C6 = new System.Windows.Forms.PictureBox();
			this.pB_R4C6 = new System.Windows.Forms.PictureBox();
			this.pB_R4C5 = new System.Windows.Forms.PictureBox();
			this.pB_R4C4 = new System.Windows.Forms.PictureBox();
			this.pB_R4C3 = new System.Windows.Forms.PictureBox();
			this.pB_R4C2 = new System.Windows.Forms.PictureBox();
			this.pB_R4C1 = new System.Windows.Forms.PictureBox();
			this.pB_R4C0 = new System.Windows.Forms.PictureBox();
			this.pB_R3C6 = new System.Windows.Forms.PictureBox();
			this.pB_R3C5 = new System.Windows.Forms.PictureBox();
			this.pB_R3C4 = new System.Windows.Forms.PictureBox();
			this.pB_R3C3 = new System.Windows.Forms.PictureBox();
			this.pB_R3C2 = new System.Windows.Forms.PictureBox();
			this.pB_R3C1 = new System.Windows.Forms.PictureBox();
			this.pB_R3C0 = new System.Windows.Forms.PictureBox();
			this.pB_R0C6 = new System.Windows.Forms.PictureBox();
			this.pB_R0C5 = new System.Windows.Forms.PictureBox();
			this.pB_R0C4 = new System.Windows.Forms.PictureBox();
			this.pB_R0C3 = new System.Windows.Forms.PictureBox();
			this.pB_R0C2 = new System.Windows.Forms.PictureBox();
			this.pB_R0C1 = new System.Windows.Forms.PictureBox();
			this.pB_R0C0 = new System.Windows.Forms.PictureBox();
			this.pB_R1C6 = new System.Windows.Forms.PictureBox();
			this.pB_R1C5 = new System.Windows.Forms.PictureBox();
			this.pB_R1C4 = new System.Windows.Forms.PictureBox();
			this.pB_R1C3 = new System.Windows.Forms.PictureBox();
			this.pB_R1C2 = new System.Windows.Forms.PictureBox();
			this.pB_R1C1 = new System.Windows.Forms.PictureBox();
			this.pB_R1C0 = new System.Windows.Forms.PictureBox();
			this.pB_R2C6 = new System.Windows.Forms.PictureBox();
			this.pB_R2C5 = new System.Windows.Forms.PictureBox();
			this.pB_R2C4 = new System.Windows.Forms.PictureBox();
			this.pB_R2C3 = new System.Windows.Forms.PictureBox();
			this.pB_R2C2 = new System.Windows.Forms.PictureBox();
			this.pB_R2C1 = new System.Windows.Forms.PictureBox();
			this.pB_R2C0 = new System.Windows.Forms.PictureBox();
			this.pictureBox1 = new System.Windows.Forms.PictureBox();
			this.pictureBox2 = new System.Windows.Forms.PictureBox();
			this.pictureBox3 = new System.Windows.Forms.PictureBox();
			this.pictureBox4 = new System.Windows.Forms.PictureBox();
			this.pictureBox5 = new System.Windows.Forms.PictureBox();
			this.pictureBox6 = new System.Windows.Forms.PictureBox();
			this.pictureBox7 = new System.Windows.Forms.PictureBox();
			((System.ComponentModel.ISupportInitialize)(this.pB_R5C0)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pB_R5C1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pB_R5C2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pB_R5C3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pB_R5C4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pB_R5C5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pB_R5C6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pB_R4C6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pB_R4C5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pB_R4C4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pB_R4C3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pB_R4C2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pB_R4C1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pB_R4C0)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pB_R3C6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pB_R3C5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pB_R3C4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pB_R3C3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pB_R3C2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pB_R3C1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pB_R3C0)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pB_R0C6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pB_R0C5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pB_R0C4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pB_R0C3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pB_R0C2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pB_R0C1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pB_R0C0)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pB_R1C6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pB_R1C5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pB_R1C4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pB_R1C3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pB_R1C2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pB_R1C1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pB_R1C0)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pB_R2C6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pB_R2C5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pB_R2C4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pB_R2C3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pB_R2C2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pB_R2C1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pB_R2C0)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
			this.SuspendLayout();
			// 
			// pB_R5C0
			// 
			this.pB_R5C0.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.pB_R5C0.BackColor = System.Drawing.Color.Transparent;
			this.pB_R5C0.ErrorImage = null;
			this.pB_R5C0.InitialImage = null;
			this.pB_R5C0.Location = new System.Drawing.Point(206, 613);
			this.pB_R5C0.Name = "pB_R5C0";
			this.pB_R5C0.Size = new System.Drawing.Size(70, 70);
			this.pB_R5C0.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
			this.pB_R5C0.TabIndex = 52;
			this.pB_R5C0.TabStop = false;
			// 
			// pB_R5C1
			// 
			this.pB_R5C1.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.pB_R5C1.BackColor = System.Drawing.Color.Transparent;
			this.pB_R5C1.ErrorImage = null;
			this.pB_R5C1.InitialImage = null;
			this.pB_R5C1.Location = new System.Drawing.Point(296, 613);
			this.pB_R5C1.Name = "pB_R5C1";
			this.pB_R5C1.Size = new System.Drawing.Size(70, 70);
			this.pB_R5C1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
			this.pB_R5C1.TabIndex = 53;
			this.pB_R5C1.TabStop = false;
			// 
			// pB_R5C2
			// 
			this.pB_R5C2.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.pB_R5C2.BackColor = System.Drawing.Color.Transparent;
			this.pB_R5C2.InitialImage = null;
			this.pB_R5C2.Location = new System.Drawing.Point(386, 613);
			this.pB_R5C2.Name = "pB_R5C2";
			this.pB_R5C2.Size = new System.Drawing.Size(70, 70);
			this.pB_R5C2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
			this.pB_R5C2.TabIndex = 54;
			this.pB_R5C2.TabStop = false;
			// 
			// pB_R5C3
			// 
			this.pB_R5C3.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.pB_R5C3.BackColor = System.Drawing.Color.Transparent;
			this.pB_R5C3.InitialImage = null;
			this.pB_R5C3.Location = new System.Drawing.Point(476, 613);
			this.pB_R5C3.Name = "pB_R5C3";
			this.pB_R5C3.Size = new System.Drawing.Size(70, 70);
			this.pB_R5C3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
			this.pB_R5C3.TabIndex = 55;
			this.pB_R5C3.TabStop = false;
			// 
			// pB_R5C4
			// 
			this.pB_R5C4.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.pB_R5C4.BackColor = System.Drawing.Color.Transparent;
			this.pB_R5C4.InitialImage = null;
			this.pB_R5C4.Location = new System.Drawing.Point(566, 613);
			this.pB_R5C4.Name = "pB_R5C4";
			this.pB_R5C4.Size = new System.Drawing.Size(70, 70);
			this.pB_R5C4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
			this.pB_R5C4.TabIndex = 56;
			this.pB_R5C4.TabStop = false;
			// 
			// pB_R5C5
			// 
			this.pB_R5C5.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.pB_R5C5.BackColor = System.Drawing.Color.Transparent;
			this.pB_R5C5.InitialImage = null;
			this.pB_R5C5.Location = new System.Drawing.Point(656, 613);
			this.pB_R5C5.Name = "pB_R5C5";
			this.pB_R5C5.Size = new System.Drawing.Size(70, 70);
			this.pB_R5C5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
			this.pB_R5C5.TabIndex = 57;
			this.pB_R5C5.TabStop = false;
			// 
			// pB_R5C6
			// 
			this.pB_R5C6.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.pB_R5C6.BackColor = System.Drawing.Color.Transparent;
			this.pB_R5C6.InitialImage = null;
			this.pB_R5C6.Location = new System.Drawing.Point(746, 613);
			this.pB_R5C6.Name = "pB_R5C6";
			this.pB_R5C6.Size = new System.Drawing.Size(70, 70);
			this.pB_R5C6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
			this.pB_R5C6.TabIndex = 58;
			this.pB_R5C6.TabStop = false;
			// 
			// pB_R4C6
			// 
			this.pB_R4C6.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.pB_R4C6.BackColor = System.Drawing.Color.Transparent;
			this.pB_R4C6.InitialImage = null;
			this.pB_R4C6.Location = new System.Drawing.Point(746, 533);
			this.pB_R4C6.Name = "pB_R4C6";
			this.pB_R4C6.Size = new System.Drawing.Size(70, 70);
			this.pB_R4C6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
			this.pB_R4C6.TabIndex = 65;
			this.pB_R4C6.TabStop = false;
			// 
			// pB_R4C5
			// 
			this.pB_R4C5.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.pB_R4C5.BackColor = System.Drawing.Color.Transparent;
			this.pB_R4C5.InitialImage = null;
			this.pB_R4C5.Location = new System.Drawing.Point(656, 533);
			this.pB_R4C5.Name = "pB_R4C5";
			this.pB_R4C5.Size = new System.Drawing.Size(70, 70);
			this.pB_R4C5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
			this.pB_R4C5.TabIndex = 64;
			this.pB_R4C5.TabStop = false;
			// 
			// pB_R4C4
			// 
			this.pB_R4C4.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.pB_R4C4.BackColor = System.Drawing.Color.Transparent;
			this.pB_R4C4.InitialImage = null;
			this.pB_R4C4.Location = new System.Drawing.Point(566, 533);
			this.pB_R4C4.Name = "pB_R4C4";
			this.pB_R4C4.Size = new System.Drawing.Size(70, 70);
			this.pB_R4C4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
			this.pB_R4C4.TabIndex = 63;
			this.pB_R4C4.TabStop = false;
			// 
			// pB_R4C3
			// 
			this.pB_R4C3.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.pB_R4C3.BackColor = System.Drawing.Color.Transparent;
			this.pB_R4C3.InitialImage = null;
			this.pB_R4C3.Location = new System.Drawing.Point(476, 533);
			this.pB_R4C3.Name = "pB_R4C3";
			this.pB_R4C3.Size = new System.Drawing.Size(70, 70);
			this.pB_R4C3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
			this.pB_R4C3.TabIndex = 62;
			this.pB_R4C3.TabStop = false;
			// 
			// pB_R4C2
			// 
			this.pB_R4C2.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.pB_R4C2.BackColor = System.Drawing.Color.Transparent;
			this.pB_R4C2.InitialImage = null;
			this.pB_R4C2.Location = new System.Drawing.Point(386, 533);
			this.pB_R4C2.Name = "pB_R4C2";
			this.pB_R4C2.Size = new System.Drawing.Size(70, 70);
			this.pB_R4C2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
			this.pB_R4C2.TabIndex = 61;
			this.pB_R4C2.TabStop = false;
			// 
			// pB_R4C1
			// 
			this.pB_R4C1.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.pB_R4C1.BackColor = System.Drawing.Color.Transparent;
			this.pB_R4C1.InitialImage = null;
			this.pB_R4C1.Location = new System.Drawing.Point(296, 533);
			this.pB_R4C1.Name = "pB_R4C1";
			this.pB_R4C1.Size = new System.Drawing.Size(70, 70);
			this.pB_R4C1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
			this.pB_R4C1.TabIndex = 60;
			this.pB_R4C1.TabStop = false;
			// 
			// pB_R4C0
			// 
			this.pB_R4C0.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.pB_R4C0.BackColor = System.Drawing.Color.Transparent;
			this.pB_R4C0.InitialImage = null;
			this.pB_R4C0.Location = new System.Drawing.Point(206, 533);
			this.pB_R4C0.Name = "pB_R4C0";
			this.pB_R4C0.Size = new System.Drawing.Size(70, 70);
			this.pB_R4C0.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
			this.pB_R4C0.TabIndex = 59;
			this.pB_R4C0.TabStop = false;
			// 
			// pB_R3C6
			// 
			this.pB_R3C6.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.pB_R3C6.BackColor = System.Drawing.Color.Transparent;
			this.pB_R3C6.InitialImage = null;
			this.pB_R3C6.Location = new System.Drawing.Point(746, 453);
			this.pB_R3C6.Name = "pB_R3C6";
			this.pB_R3C6.Size = new System.Drawing.Size(70, 70);
			this.pB_R3C6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
			this.pB_R3C6.TabIndex = 72;
			this.pB_R3C6.TabStop = false;
			// 
			// pB_R3C5
			// 
			this.pB_R3C5.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.pB_R3C5.BackColor = System.Drawing.Color.Transparent;
			this.pB_R3C5.InitialImage = null;
			this.pB_R3C5.Location = new System.Drawing.Point(656, 453);
			this.pB_R3C5.Name = "pB_R3C5";
			this.pB_R3C5.Size = new System.Drawing.Size(70, 70);
			this.pB_R3C5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
			this.pB_R3C5.TabIndex = 71;
			this.pB_R3C5.TabStop = false;
			// 
			// pB_R3C4
			// 
			this.pB_R3C4.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.pB_R3C4.BackColor = System.Drawing.Color.Transparent;
			this.pB_R3C4.InitialImage = null;
			this.pB_R3C4.Location = new System.Drawing.Point(566, 453);
			this.pB_R3C4.Name = "pB_R3C4";
			this.pB_R3C4.Size = new System.Drawing.Size(70, 70);
			this.pB_R3C4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
			this.pB_R3C4.TabIndex = 70;
			this.pB_R3C4.TabStop = false;
			// 
			// pB_R3C3
			// 
			this.pB_R3C3.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.pB_R3C3.BackColor = System.Drawing.Color.Transparent;
			this.pB_R3C3.InitialImage = null;
			this.pB_R3C3.Location = new System.Drawing.Point(476, 453);
			this.pB_R3C3.Name = "pB_R3C3";
			this.pB_R3C3.Size = new System.Drawing.Size(70, 70);
			this.pB_R3C3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
			this.pB_R3C3.TabIndex = 69;
			this.pB_R3C3.TabStop = false;
			// 
			// pB_R3C2
			// 
			this.pB_R3C2.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.pB_R3C2.BackColor = System.Drawing.Color.Transparent;
			this.pB_R3C2.InitialImage = null;
			this.pB_R3C2.Location = new System.Drawing.Point(386, 453);
			this.pB_R3C2.Name = "pB_R3C2";
			this.pB_R3C2.Size = new System.Drawing.Size(70, 70);
			this.pB_R3C2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
			this.pB_R3C2.TabIndex = 68;
			this.pB_R3C2.TabStop = false;
			// 
			// pB_R3C1
			// 
			this.pB_R3C1.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.pB_R3C1.BackColor = System.Drawing.Color.Transparent;
			this.pB_R3C1.InitialImage = null;
			this.pB_R3C1.Location = new System.Drawing.Point(296, 453);
			this.pB_R3C1.Name = "pB_R3C1";
			this.pB_R3C1.Size = new System.Drawing.Size(70, 70);
			this.pB_R3C1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
			this.pB_R3C1.TabIndex = 67;
			this.pB_R3C1.TabStop = false;
			// 
			// pB_R3C0
			// 
			this.pB_R3C0.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.pB_R3C0.BackColor = System.Drawing.Color.Transparent;
			this.pB_R3C0.InitialImage = null;
			this.pB_R3C0.Location = new System.Drawing.Point(206, 453);
			this.pB_R3C0.Name = "pB_R3C0";
			this.pB_R3C0.Size = new System.Drawing.Size(70, 70);
			this.pB_R3C0.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
			this.pB_R3C0.TabIndex = 66;
			this.pB_R3C0.TabStop = false;
			// 
			// pB_R0C6
			// 
			this.pB_R0C6.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.pB_R0C6.BackColor = System.Drawing.Color.Transparent;
			this.pB_R0C6.ErrorImage = null;
			this.pB_R0C6.InitialImage = null;
			this.pB_R0C6.Location = new System.Drawing.Point(746, 213);
			this.pB_R0C6.Name = "pB_R0C6";
			this.pB_R0C6.Size = new System.Drawing.Size(70, 70);
			this.pB_R0C6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
			this.pB_R0C6.TabIndex = 93;
			this.pB_R0C6.TabStop = false;
			// 
			// pB_R0C5
			// 
			this.pB_R0C5.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.pB_R0C5.BackColor = System.Drawing.Color.Transparent;
			this.pB_R0C5.InitialImage = null;
			this.pB_R0C5.Location = new System.Drawing.Point(656, 213);
			this.pB_R0C5.Name = "pB_R0C5";
			this.pB_R0C5.Size = new System.Drawing.Size(70, 70);
			this.pB_R0C5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
			this.pB_R0C5.TabIndex = 92;
			this.pB_R0C5.TabStop = false;
			// 
			// pB_R0C4
			// 
			this.pB_R0C4.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.pB_R0C4.BackColor = System.Drawing.Color.Transparent;
			this.pB_R0C4.InitialImage = null;
			this.pB_R0C4.Location = new System.Drawing.Point(566, 213);
			this.pB_R0C4.Name = "pB_R0C4";
			this.pB_R0C4.Size = new System.Drawing.Size(70, 70);
			this.pB_R0C4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
			this.pB_R0C4.TabIndex = 91;
			this.pB_R0C4.TabStop = false;
			// 
			// pB_R0C3
			// 
			this.pB_R0C3.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.pB_R0C3.BackColor = System.Drawing.Color.Transparent;
			this.pB_R0C3.InitialImage = null;
			this.pB_R0C3.Location = new System.Drawing.Point(476, 213);
			this.pB_R0C3.Name = "pB_R0C3";
			this.pB_R0C3.Size = new System.Drawing.Size(70, 70);
			this.pB_R0C3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
			this.pB_R0C3.TabIndex = 90;
			this.pB_R0C3.TabStop = false;
			// 
			// pB_R0C2
			// 
			this.pB_R0C2.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.pB_R0C2.BackColor = System.Drawing.Color.Transparent;
			this.pB_R0C2.InitialImage = null;
			this.pB_R0C2.Location = new System.Drawing.Point(386, 213);
			this.pB_R0C2.Name = "pB_R0C2";
			this.pB_R0C2.Size = new System.Drawing.Size(70, 70);
			this.pB_R0C2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
			this.pB_R0C2.TabIndex = 89;
			this.pB_R0C2.TabStop = false;
			// 
			// pB_R0C1
			// 
			this.pB_R0C1.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.pB_R0C1.BackColor = System.Drawing.Color.Transparent;
			this.pB_R0C1.InitialImage = null;
			this.pB_R0C1.Location = new System.Drawing.Point(296, 213);
			this.pB_R0C1.Name = "pB_R0C1";
			this.pB_R0C1.Size = new System.Drawing.Size(70, 70);
			this.pB_R0C1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
			this.pB_R0C1.TabIndex = 88;
			this.pB_R0C1.TabStop = false;
			// 
			// pB_R0C0
			// 
			this.pB_R0C0.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.pB_R0C0.BackColor = System.Drawing.Color.Transparent;
			this.pB_R0C0.InitialImage = null;
			this.pB_R0C0.Location = new System.Drawing.Point(206, 213);
			this.pB_R0C0.Name = "pB_R0C0";
			this.pB_R0C0.Size = new System.Drawing.Size(70, 70);
			this.pB_R0C0.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
			this.pB_R0C0.TabIndex = 87;
			this.pB_R0C0.TabStop = false;
			// 
			// pB_R1C6
			// 
			this.pB_R1C6.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.pB_R1C6.BackColor = System.Drawing.Color.Transparent;
			this.pB_R1C6.InitialImage = null;
			this.pB_R1C6.Location = new System.Drawing.Point(746, 293);
			this.pB_R1C6.Name = "pB_R1C6";
			this.pB_R1C6.Size = new System.Drawing.Size(70, 70);
			this.pB_R1C6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
			this.pB_R1C6.TabIndex = 86;
			this.pB_R1C6.TabStop = false;
			// 
			// pB_R1C5
			// 
			this.pB_R1C5.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.pB_R1C5.BackColor = System.Drawing.Color.Transparent;
			this.pB_R1C5.InitialImage = null;
			this.pB_R1C5.Location = new System.Drawing.Point(656, 293);
			this.pB_R1C5.Name = "pB_R1C5";
			this.pB_R1C5.Size = new System.Drawing.Size(70, 70);
			this.pB_R1C5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
			this.pB_R1C5.TabIndex = 85;
			this.pB_R1C5.TabStop = false;
			// 
			// pB_R1C4
			// 
			this.pB_R1C4.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.pB_R1C4.BackColor = System.Drawing.Color.Transparent;
			this.pB_R1C4.InitialImage = null;
			this.pB_R1C4.Location = new System.Drawing.Point(566, 293);
			this.pB_R1C4.Name = "pB_R1C4";
			this.pB_R1C4.Size = new System.Drawing.Size(70, 70);
			this.pB_R1C4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
			this.pB_R1C4.TabIndex = 84;
			this.pB_R1C4.TabStop = false;
			// 
			// pB_R1C3
			// 
			this.pB_R1C3.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.pB_R1C3.BackColor = System.Drawing.Color.Transparent;
			this.pB_R1C3.InitialImage = null;
			this.pB_R1C3.Location = new System.Drawing.Point(476, 293);
			this.pB_R1C3.Name = "pB_R1C3";
			this.pB_R1C3.Size = new System.Drawing.Size(70, 70);
			this.pB_R1C3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
			this.pB_R1C3.TabIndex = 83;
			this.pB_R1C3.TabStop = false;
			// 
			// pB_R1C2
			// 
			this.pB_R1C2.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.pB_R1C2.BackColor = System.Drawing.Color.Transparent;
			this.pB_R1C2.InitialImage = null;
			this.pB_R1C2.Location = new System.Drawing.Point(386, 293);
			this.pB_R1C2.Name = "pB_R1C2";
			this.pB_R1C2.Size = new System.Drawing.Size(70, 70);
			this.pB_R1C2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
			this.pB_R1C2.TabIndex = 82;
			this.pB_R1C2.TabStop = false;
			// 
			// pB_R1C1
			// 
			this.pB_R1C1.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.pB_R1C1.BackColor = System.Drawing.Color.Transparent;
			this.pB_R1C1.InitialImage = null;
			this.pB_R1C1.Location = new System.Drawing.Point(296, 293);
			this.pB_R1C1.Name = "pB_R1C1";
			this.pB_R1C1.Size = new System.Drawing.Size(70, 70);
			this.pB_R1C1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
			this.pB_R1C1.TabIndex = 81;
			this.pB_R1C1.TabStop = false;
			// 
			// pB_R1C0
			// 
			this.pB_R1C0.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.pB_R1C0.BackColor = System.Drawing.Color.Transparent;
			this.pB_R1C0.InitialImage = null;
			this.pB_R1C0.Location = new System.Drawing.Point(206, 293);
			this.pB_R1C0.Name = "pB_R1C0";
			this.pB_R1C0.Size = new System.Drawing.Size(70, 70);
			this.pB_R1C0.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
			this.pB_R1C0.TabIndex = 80;
			this.pB_R1C0.TabStop = false;
			// 
			// pB_R2C6
			// 
			this.pB_R2C6.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.pB_R2C6.BackColor = System.Drawing.Color.Transparent;
			this.pB_R2C6.InitialImage = null;
			this.pB_R2C6.Location = new System.Drawing.Point(746, 373);
			this.pB_R2C6.Name = "pB_R2C6";
			this.pB_R2C6.Size = new System.Drawing.Size(70, 70);
			this.pB_R2C6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
			this.pB_R2C6.TabIndex = 79;
			this.pB_R2C6.TabStop = false;
			// 
			// pB_R2C5
			// 
			this.pB_R2C5.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.pB_R2C5.BackColor = System.Drawing.Color.Transparent;
			this.pB_R2C5.InitialImage = null;
			this.pB_R2C5.Location = new System.Drawing.Point(656, 373);
			this.pB_R2C5.Name = "pB_R2C5";
			this.pB_R2C5.Size = new System.Drawing.Size(70, 70);
			this.pB_R2C5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
			this.pB_R2C5.TabIndex = 78;
			this.pB_R2C5.TabStop = false;
			// 
			// pB_R2C4
			// 
			this.pB_R2C4.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.pB_R2C4.BackColor = System.Drawing.Color.Transparent;
			this.pB_R2C4.InitialImage = null;
			this.pB_R2C4.Location = new System.Drawing.Point(566, 373);
			this.pB_R2C4.Name = "pB_R2C4";
			this.pB_R2C4.Size = new System.Drawing.Size(70, 70);
			this.pB_R2C4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
			this.pB_R2C4.TabIndex = 77;
			this.pB_R2C4.TabStop = false;
			// 
			// pB_R2C3
			// 
			this.pB_R2C3.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.pB_R2C3.BackColor = System.Drawing.Color.Transparent;
			this.pB_R2C3.InitialImage = null;
			this.pB_R2C3.Location = new System.Drawing.Point(476, 373);
			this.pB_R2C3.Name = "pB_R2C3";
			this.pB_R2C3.Size = new System.Drawing.Size(70, 70);
			this.pB_R2C3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
			this.pB_R2C3.TabIndex = 76;
			this.pB_R2C3.TabStop = false;
			// 
			// pB_R2C2
			// 
			this.pB_R2C2.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.pB_R2C2.BackColor = System.Drawing.Color.Transparent;
			this.pB_R2C2.InitialImage = null;
			this.pB_R2C2.Location = new System.Drawing.Point(386, 373);
			this.pB_R2C2.Name = "pB_R2C2";
			this.pB_R2C2.Size = new System.Drawing.Size(70, 70);
			this.pB_R2C2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
			this.pB_R2C2.TabIndex = 75;
			this.pB_R2C2.TabStop = false;
			// 
			// pB_R2C1
			// 
			this.pB_R2C1.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.pB_R2C1.BackColor = System.Drawing.Color.Transparent;
			this.pB_R2C1.InitialImage = null;
			this.pB_R2C1.Location = new System.Drawing.Point(296, 373);
			this.pB_R2C1.Name = "pB_R2C1";
			this.pB_R2C1.Size = new System.Drawing.Size(70, 70);
			this.pB_R2C1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
			this.pB_R2C1.TabIndex = 74;
			this.pB_R2C1.TabStop = false;
			// 
			// pB_R2C0
			// 
			this.pB_R2C0.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.pB_R2C0.BackColor = System.Drawing.Color.Transparent;
			this.pB_R2C0.InitialImage = null;
			this.pB_R2C0.Location = new System.Drawing.Point(206, 373);
			this.pB_R2C0.Name = "pB_R2C0";
			this.pB_R2C0.Size = new System.Drawing.Size(70, 70);
			this.pB_R2C0.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
			this.pB_R2C0.TabIndex = 73;
			this.pB_R2C0.TabStop = false;
			// 
			// pictureBox1
			// 
			this.pictureBox1.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
			this.pictureBox1.ErrorImage = null;
			this.pictureBox1.InitialImage = null;
			this.pictureBox1.Location = new System.Drawing.Point(206, 137);
			this.pictureBox1.Name = "pictureBox1";
			this.pictureBox1.Size = new System.Drawing.Size(70, 70);
			this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
			this.pictureBox1.TabIndex = 94;
			this.pictureBox1.TabStop = false;
			this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
			this.pictureBox1.MouseEnter += new System.EventHandler(this.pictureBox1_MouseEnter);
			this.pictureBox1.MouseLeave += new System.EventHandler(this.pictureBox1_MouseLeave);
			// 
			// pictureBox2
			// 
			this.pictureBox2.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.pictureBox2.BackColor = System.Drawing.Color.Transparent;
			this.pictureBox2.ErrorImage = null;
			this.pictureBox2.InitialImage = null;
			this.pictureBox2.Location = new System.Drawing.Point(296, 137);
			this.pictureBox2.Name = "pictureBox2";
			this.pictureBox2.Size = new System.Drawing.Size(70, 70);
			this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
			this.pictureBox2.TabIndex = 95;
			this.pictureBox2.TabStop = false;
			this.pictureBox2.Click += new System.EventHandler(this.pictureBox2_Click);
			this.pictureBox2.MouseEnter += new System.EventHandler(this.pictureBox2_MouseEnter);
			this.pictureBox2.MouseLeave += new System.EventHandler(this.pictureBox2_MouseLeave);
			// 
			// pictureBox3
			// 
			this.pictureBox3.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.pictureBox3.BackColor = System.Drawing.Color.Transparent;
			this.pictureBox3.ErrorImage = null;
			this.pictureBox3.InitialImage = null;
			this.pictureBox3.Location = new System.Drawing.Point(386, 137);
			this.pictureBox3.Name = "pictureBox3";
			this.pictureBox3.Size = new System.Drawing.Size(70, 70);
			this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
			this.pictureBox3.TabIndex = 96;
			this.pictureBox3.TabStop = false;
			this.pictureBox3.Click += new System.EventHandler(this.pictureBox3_Click);
			this.pictureBox3.MouseEnter += new System.EventHandler(this.pictureBox3_MouseEnter);
			this.pictureBox3.MouseLeave += new System.EventHandler(this.pictureBox3_MouseLeave);
			// 
			// pictureBox4
			// 
			this.pictureBox4.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.pictureBox4.BackColor = System.Drawing.Color.Transparent;
			this.pictureBox4.ErrorImage = null;
			this.pictureBox4.InitialImage = null;
			this.pictureBox4.Location = new System.Drawing.Point(476, 137);
			this.pictureBox4.Name = "pictureBox4";
			this.pictureBox4.Size = new System.Drawing.Size(70, 70);
			this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
			this.pictureBox4.TabIndex = 97;
			this.pictureBox4.TabStop = false;
			this.pictureBox4.Click += new System.EventHandler(this.pictureBox4_Click);
			this.pictureBox4.MouseEnter += new System.EventHandler(this.pictureBox4_MouseEnter);
			this.pictureBox4.MouseLeave += new System.EventHandler(this.pictureBox4_MouseLeave);
			// 
			// pictureBox5
			// 
			this.pictureBox5.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.pictureBox5.BackColor = System.Drawing.Color.Transparent;
			this.pictureBox5.ErrorImage = null;
			this.pictureBox5.InitialImage = null;
			this.pictureBox5.Location = new System.Drawing.Point(566, 137);
			this.pictureBox5.Name = "pictureBox5";
			this.pictureBox5.Size = new System.Drawing.Size(70, 70);
			this.pictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
			this.pictureBox5.TabIndex = 98;
			this.pictureBox5.TabStop = false;
			this.pictureBox5.Click += new System.EventHandler(this.pictureBox5_Click);
			this.pictureBox5.MouseEnter += new System.EventHandler(this.pictureBox5_MouseEnter);
			this.pictureBox5.MouseLeave += new System.EventHandler(this.pictureBox5_MouseLeave);
			// 
			// pictureBox6
			// 
			this.pictureBox6.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.pictureBox6.BackColor = System.Drawing.Color.Transparent;
			this.pictureBox6.ErrorImage = null;
			this.pictureBox6.InitialImage = null;
			this.pictureBox6.Location = new System.Drawing.Point(656, 137);
			this.pictureBox6.Name = "pictureBox6";
			this.pictureBox6.Size = new System.Drawing.Size(70, 70);
			this.pictureBox6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
			this.pictureBox6.TabIndex = 99;
			this.pictureBox6.TabStop = false;
			this.pictureBox6.Click += new System.EventHandler(this.pictureBox6_Click);
			this.pictureBox6.MouseEnter += new System.EventHandler(this.pictureBox6_MouseEnter);
			this.pictureBox6.MouseLeave += new System.EventHandler(this.pictureBox6_MouseLeave);
			// 
			// pictureBox7
			// 
			this.pictureBox7.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.pictureBox7.BackColor = System.Drawing.Color.Transparent;
			this.pictureBox7.ErrorImage = null;
			this.pictureBox7.InitialImage = null;
			this.pictureBox7.Location = new System.Drawing.Point(746, 137);
			this.pictureBox7.Name = "pictureBox7";
			this.pictureBox7.Size = new System.Drawing.Size(70, 70);
			this.pictureBox7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
			this.pictureBox7.TabIndex = 100;
			this.pictureBox7.TabStop = false;
			this.pictureBox7.Click += new System.EventHandler(this.pictureBox7_Click);
			this.pictureBox7.MouseEnter += new System.EventHandler(this.pictureBox7_MouseEnter);
			this.pictureBox7.MouseLeave += new System.EventHandler(this.pictureBox7_MouseLeave);
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 22F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
			this.ClientSize = new System.Drawing.Size(1022, 894);
			this.Controls.Add(this.pictureBox7);
			this.Controls.Add(this.pictureBox6);
			this.Controls.Add(this.pictureBox5);
			this.Controls.Add(this.pictureBox4);
			this.Controls.Add(this.pictureBox3);
			this.Controls.Add(this.pictureBox2);
			this.Controls.Add(this.pictureBox1);
			this.Controls.Add(this.pB_R0C6);
			this.Controls.Add(this.pB_R0C5);
			this.Controls.Add(this.pB_R0C4);
			this.Controls.Add(this.pB_R0C3);
			this.Controls.Add(this.pB_R0C2);
			this.Controls.Add(this.pB_R0C1);
			this.Controls.Add(this.pB_R0C0);
			this.Controls.Add(this.pB_R1C6);
			this.Controls.Add(this.pB_R1C5);
			this.Controls.Add(this.pB_R1C4);
			this.Controls.Add(this.pB_R1C3);
			this.Controls.Add(this.pB_R1C2);
			this.Controls.Add(this.pB_R1C1);
			this.Controls.Add(this.pB_R1C0);
			this.Controls.Add(this.pB_R2C6);
			this.Controls.Add(this.pB_R2C5);
			this.Controls.Add(this.pB_R2C4);
			this.Controls.Add(this.pB_R2C3);
			this.Controls.Add(this.pB_R2C2);
			this.Controls.Add(this.pB_R2C1);
			this.Controls.Add(this.pB_R2C0);
			this.Controls.Add(this.pB_R3C6);
			this.Controls.Add(this.pB_R3C5);
			this.Controls.Add(this.pB_R3C4);
			this.Controls.Add(this.pB_R3C3);
			this.Controls.Add(this.pB_R3C2);
			this.Controls.Add(this.pB_R3C1);
			this.Controls.Add(this.pB_R3C0);
			this.Controls.Add(this.pB_R4C6);
			this.Controls.Add(this.pB_R4C5);
			this.Controls.Add(this.pB_R4C4);
			this.Controls.Add(this.pB_R4C3);
			this.Controls.Add(this.pB_R4C2);
			this.Controls.Add(this.pB_R4C1);
			this.Controls.Add(this.pB_R4C0);
			this.Controls.Add(this.pB_R5C6);
			this.Controls.Add(this.pB_R5C5);
			this.Controls.Add(this.pB_R5C4);
			this.Controls.Add(this.pB_R5C3);
			this.Controls.Add(this.pB_R5C2);
			this.Controls.Add(this.pB_R5C1);
			this.Controls.Add(this.pB_R5C0);
			this.DoubleBuffered = true;
			this.Name = "Form1";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Connect 4!";
			((System.ComponentModel.ISupportInitialize)(this.pB_R5C0)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pB_R5C1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pB_R5C2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pB_R5C3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pB_R5C4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pB_R5C5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pB_R5C6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pB_R4C6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pB_R4C5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pB_R4C4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pB_R4C3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pB_R4C2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pB_R4C1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pB_R4C0)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pB_R3C6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pB_R3C5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pB_R3C4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pB_R3C3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pB_R3C2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pB_R3C1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pB_R3C0)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pB_R0C6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pB_R0C5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pB_R0C4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pB_R0C3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pB_R0C2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pB_R0C1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pB_R0C0)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pB_R1C6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pB_R1C5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pB_R1C4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pB_R1C3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pB_R1C2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pB_R1C1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pB_R1C0)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pB_R2C6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pB_R2C5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pB_R2C4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pB_R2C3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pB_R2C2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pB_R2C1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pB_R2C0)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.PictureBox pB_R5C0;
		private System.Windows.Forms.PictureBox pB_R5C1;
		private System.Windows.Forms.PictureBox pB_R5C2;
		private System.Windows.Forms.PictureBox pB_R5C3;
		private System.Windows.Forms.PictureBox pB_R5C4;
		private System.Windows.Forms.PictureBox pB_R5C5;
		private System.Windows.Forms.PictureBox pB_R5C6;
		private System.Windows.Forms.PictureBox pB_R4C6;
		private System.Windows.Forms.PictureBox pB_R4C5;
		private System.Windows.Forms.PictureBox pB_R4C4;
		private System.Windows.Forms.PictureBox pB_R4C3;
		private System.Windows.Forms.PictureBox pB_R4C2;
		private System.Windows.Forms.PictureBox pB_R4C1;
		private System.Windows.Forms.PictureBox pB_R4C0;
		private System.Windows.Forms.PictureBox pB_R3C6;
		private System.Windows.Forms.PictureBox pB_R3C5;
		private System.Windows.Forms.PictureBox pB_R3C4;
		private System.Windows.Forms.PictureBox pB_R3C3;
		private System.Windows.Forms.PictureBox pB_R3C2;
		private System.Windows.Forms.PictureBox pB_R3C1;
		private System.Windows.Forms.PictureBox pB_R3C0;
		private System.Windows.Forms.PictureBox pB_R0C6;
		private System.Windows.Forms.PictureBox pB_R0C5;
		private System.Windows.Forms.PictureBox pB_R0C4;
		private System.Windows.Forms.PictureBox pB_R0C3;
		private System.Windows.Forms.PictureBox pB_R0C2;
		private System.Windows.Forms.PictureBox pB_R0C1;
		private System.Windows.Forms.PictureBox pB_R0C0;
		private System.Windows.Forms.PictureBox pB_R1C6;
		private System.Windows.Forms.PictureBox pB_R1C5;
		private System.Windows.Forms.PictureBox pB_R1C4;
		private System.Windows.Forms.PictureBox pB_R1C3;
		private System.Windows.Forms.PictureBox pB_R1C2;
		private System.Windows.Forms.PictureBox pB_R1C1;
		private System.Windows.Forms.PictureBox pB_R1C0;
		private System.Windows.Forms.PictureBox pB_R2C6;
		private System.Windows.Forms.PictureBox pB_R2C5;
		private System.Windows.Forms.PictureBox pB_R2C4;
		private System.Windows.Forms.PictureBox pB_R2C3;
		private System.Windows.Forms.PictureBox pB_R2C2;
		private System.Windows.Forms.PictureBox pB_R2C1;
		private System.Windows.Forms.PictureBox pB_R2C0;
		private System.Windows.Forms.PictureBox pictureBox1;
		private System.Windows.Forms.PictureBox pictureBox2;
		private System.Windows.Forms.PictureBox pictureBox3;
		private System.Windows.Forms.PictureBox pictureBox4;
		private System.Windows.Forms.PictureBox pictureBox5;
		private System.Windows.Forms.PictureBox pictureBox6;
		private System.Windows.Forms.PictureBox pictureBox7;

	}
}

