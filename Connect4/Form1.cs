﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;


namespace Connect4
{
	public partial class Form1 : Form
	{
		static int turns = 1;
		private static PictureBox[,] pictureBox;


		public Form1()
		{
			InitializeComponent();

			pictureBox = new PictureBox[6, 7] 
			{
			{pB_R0C0,pB_R0C1,pB_R0C2,pB_R0C3,pB_R0C4,pB_R0C5,pB_R0C6}, //r7 [0,c]
			{pB_R1C0,pB_R1C1,pB_R1C2,pB_R1C3,pB_R1C4,pB_R1C5,pB_R1C6},//r5 [1,c]
			{pB_R2C0,pB_R2C1,pB_R2C2,pB_R2C3,pB_R2C4,pB_R2C5,pB_R2C6}, //r4 [2,c]
			{pB_R3C0,pB_R3C1,pB_R3C2,pB_R3C3,pB_R3C4,pB_R3C5,pB_R3C6}, //r3 [3,c]
			{pB_R4C0,pB_R4C1,pB_R4C2,pB_R4C3,pB_R4C4,pB_R4C5,pB_R4C6}, //r2 [4,c]
			{pB_R5C0,pB_R5C1,pB_R5C2,pB_R5C3,pB_R5C4,pB_R5C5,pB_R5C6}, //r1 [5,c]
			};

		}


		public static void UpdateGameBoard(int r, int c)
		{
			pictureBox[r, c].ImageLocation = GameBoard.gameBoard[r, c];

		}

		private void pictureBox1_Click(object sender, EventArgs e)
		{
			{
				if (GameBoard.IsLegalPlacement(0) == true)
				{
					int row = GameBoard.GetRow(0);
					GameBoard.DropPiece(0, turns, row);
					UpdateGameBoard(row, 0);

					if (GameBoard.CheckWinner(row, 0, turns) == false)
					{
						turns += 1;
						pictureBox1.ImageLocation = GameBoard.TurnDecider(turns);
						
					}

					if (GameBoard.CheckWinner(row, 0, turns) == true)
					{
						MessageBox.Show(GameBoard.NameWinner(turns));
						Form1.ClearBoard();
						turns = 1;
					}
				}
			}
		}

		private void pictureBox2_Click(object sender, EventArgs e)
		{
			{
				if (GameBoard.IsLegalPlacement(1) == true)
				{
					int row = GameBoard.GetRow(1);
					GameBoard.DropPiece(1, turns, row);
					UpdateGameBoard(row, 1);

					if (GameBoard.CheckWinner(row, 1, turns) == false)
					{
						turns += 1;
						pictureBox2.ImageLocation = GameBoard.TurnDecider(turns);
					}

					if (GameBoard.CheckWinner(row, 1, turns) == true)
					{
						MessageBox.Show(GameBoard.NameWinner(turns));
						Form1.ClearBoard();
						turns = 1;
					}
				}
			}

		}

		private void pictureBox3_Click(object sender, EventArgs e)
		{

			{
				if (GameBoard.IsLegalPlacement(2) == true)
				{
					int row = GameBoard.GetRow(2);
					GameBoard.DropPiece(2, turns, row);
					UpdateGameBoard(row, 2);

					if (GameBoard.CheckWinner(row, 2, turns) == false)
					{
						turns += 1;
						pictureBox3.ImageLocation = GameBoard.TurnDecider(turns);
					}

					if (GameBoard.CheckWinner(row, 2, turns) == true)
					{
						MessageBox.Show(GameBoard.NameWinner(turns));
						Form1.ClearBoard();
						turns = 1;
					}
				}
			}
		}

		private void pictureBox4_Click(object sender, EventArgs e)
		{
			{
				if (GameBoard.IsLegalPlacement(3) == true)
				{
					int row = GameBoard.GetRow(3);
					GameBoard.DropPiece(3, turns, row);
					UpdateGameBoard(row, 3);

					if (GameBoard.CheckWinner(row, 3, turns) == false)
					{
						turns += 1;
						pictureBox4.ImageLocation = GameBoard.TurnDecider(turns);
					}

					if (GameBoard.CheckWinner(row, 3, turns) == true)
					{
						MessageBox.Show(GameBoard.NameWinner(turns));
						Form1.ClearBoard();
						turns = 1;
					}
				}
			}
		}

		private void pictureBox5_Click(object sender, EventArgs e)
		{
			{
				if (GameBoard.IsLegalPlacement(4) == true)
				{
					int row = GameBoard.GetRow(4);
					GameBoard.DropPiece(4, turns, row);
					UpdateGameBoard(row, 4);

					if (GameBoard.CheckWinner(row, 4, turns) == false)
					{
						turns += 1;
						pictureBox5.ImageLocation = GameBoard.TurnDecider(turns);
					}

					if (GameBoard.CheckWinner(row, 4, turns) == true)
					{
						MessageBox.Show(GameBoard.NameWinner(turns));
						Form1.ClearBoard();
						turns = 1;
					}
				}
			}
		}

		private void pictureBox6_Click(object sender, EventArgs e)
		{
			{
				if (GameBoard.IsLegalPlacement(5) == true)
				{
					int row = GameBoard.GetRow(5);
					GameBoard.DropPiece(5, turns, row);
					UpdateGameBoard(row, 5);

					if (GameBoard.CheckWinner(row, 5, turns) == false)
					{
						turns += 1;
						pictureBox6.ImageLocation = GameBoard.TurnDecider(turns);
					}

					if (GameBoard.CheckWinner(row, 5, turns) == true)
					{
						MessageBox.Show(GameBoard.NameWinner(turns));
						Form1.ClearBoard();
						turns = 1;
					}
				}
			}
		}

		private void pictureBox7_Click(object sender, EventArgs e)
		{
			{
				if (GameBoard.IsLegalPlacement(6) == true)
				{
					int row = GameBoard.GetRow(6);
					GameBoard.DropPiece(6, turns, row);
					UpdateGameBoard(row, 6);

					if (GameBoard.CheckWinner(row, 6, turns) == false)
					{
						turns += 1;
						pictureBox7.ImageLocation = GameBoard.TurnDecider(turns);
					}

					if (GameBoard.CheckWinner(row, 6, turns) == true)
					{
						MessageBox.Show(GameBoard.NameWinner(turns));
						Form1.ClearBoard();
						turns = 1;
					}
				}
			}

		}

		public static void ClearBoard()
		{
			for (int r = 5; r > -1; r--)
			{
				for (int c = 6; c > -1; c--)
				{
					pictureBox[r, c].ImageLocation = null;
					GameBoard.gameBoard[r, c] = "empty";

				}
			}
			turns = 1;
		}



		private void pictureBox2_MouseEnter(object sender, EventArgs e)
		{
			pictureBox2.ImageLocation = GameBoard.TurnDecider(turns);
		}

		private void pictureBox2_MouseLeave(object sender, EventArgs e)
		{
			pictureBox2.ImageLocation = null;
		}

		private void pictureBox1_MouseEnter(object sender, EventArgs e)
		{
			pictureBox1.ImageLocation = GameBoard.TurnDecider(turns);
		}

		private void pictureBox1_MouseLeave(object sender, EventArgs e)
		{
			pictureBox1.ImageLocation = null;
		}

		private void pictureBox3_MouseEnter(object sender, EventArgs e)
		{
			pictureBox3.ImageLocation = GameBoard.TurnDecider(turns);
		}

		private void pictureBox3_MouseLeave(object sender, EventArgs e)
		{
			pictureBox3.ImageLocation = null;
		}

		private void pictureBox4_MouseEnter(object sender, EventArgs e)
		{
			pictureBox4.ImageLocation = GameBoard.TurnDecider(turns);
		}

		private void pictureBox4_MouseLeave(object sender, EventArgs e)
		{
			pictureBox4.ImageLocation = null;
		}

		private void pictureBox5_MouseEnter(object sender, EventArgs e)
		{
			pictureBox5.ImageLocation = GameBoard.TurnDecider(turns);
		}

		private void pictureBox5_MouseLeave(object sender, EventArgs e)
		{
			pictureBox5.ImageLocation = null;
		}

		private void pictureBox6_MouseEnter(object sender, EventArgs e)
		{
			pictureBox6.ImageLocation = GameBoard.TurnDecider(turns);
		}

		private void pictureBox6_MouseLeave(object sender, EventArgs e)
		{
			pictureBox6.ImageLocation = null;
		}

		private void pictureBox7_MouseEnter(object sender, EventArgs e)
		{
			pictureBox7.ImageLocation = GameBoard.TurnDecider(turns);
		}

		private void pictureBox7_MouseLeave(object sender, EventArgs e)
		{
			pictureBox7.ImageLocation = null;
		}




















	}
}
