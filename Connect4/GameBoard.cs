﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Connect4
{
	public static class GameBoard
	{
		

		public static string blackPlayer = "C:\\Users\\CB97\\Pictures\\For Game\\BlackPlayer.png";
		public static string redPlayer = "C:\\Users\\CB97\\Pictures\\For Game\\RedPlayer.png";

		public static string[,] gameBoard = new string[6, 7]  // [rows,columns]
		{  //  c1      c2    c3     c4     c5     c6     c7
		   //[r,0]	 [r,1]	[r,2]  [r,3]  [r,4]  [r,5]  [r,6]	

			{"empty","empty","empty","empty","empty","empty","empty"}, //r7 [0,c]
			{"empty","empty","empty","empty","empty","empty","empty"}, //r5 [1,c]
			{"empty","empty","empty","empty","empty","empty","empty"}, //r4 [2,c]
			{"empty","empty","empty","empty","empty","empty","empty"}, //r3 [3,c]
			{"empty","empty","empty","empty","empty","empty","empty"}, //r2 [4,c]
			{"empty","empty","empty","empty","empty","empty","empty"}, //r1 [5,c]
	
		};

		public static bool IsLegalPlacement(int column)
		{

			for (int row = 5; row > -1; row--)
			{
				string boardState = gameBoard[row, column];
				if (boardState != redPlayer && boardState != blackPlayer)
				{
					return true;
				}
			}
			return false;
		}

		public static void DropPiece(int column, int turns, int row)
		{

			gameBoard[row, column] = TurnDecider(turns);
		}

		public static int GetRow(int column)
		{
			int row;

			for (row = 5; row > -1; row--)
			{
				string boardState = gameBoard[row, column];
				if (boardState != redPlayer && boardState != blackPlayer)
				{
					break;
				}
			}

			return row;
		}

		public static string TurnDecider(int turns)
		{
			if (turns % 2 == 0)
			{
				return redPlayer;
			}

			else
				return blackPlayer;
		}

		public static string NameWinner(int turns)
		{
			if (turns % 2 == 0)
			{
				return "Red Player Wins!";
			}

			else
				return "Black Player Wins!";
		}

		public static bool CheckVertical(int row, int column, int turns)
		{

			int connected = 1;
			int initialRow = row;
			int initialColumn = column;
			string initialSlot = gameBoard[initialRow, initialColumn];
			string slotsToCheck = gameBoard[row, column];

			// check for vertical matches
			while (row < 5 && slotsToCheck == initialSlot)
			{
				row += 1;
				slotsToCheck = gameBoard[row, column];
				if (slotsToCheck == initialSlot)
					connected += 1;

				if (connected == 4)
				{
					return true;
				}
			}
			return false;



		}

		private static bool CheckLeftHorizontal(int row, int column, int turns)
		{
			//check for left horizontal matches
			int connected = 1;
			int initialRow = row;
			int initialColumn = column;
			string initialSlot = gameBoard[initialRow, initialColumn];
			string slotsToCheck = gameBoard[row, column];

			while (column > 0 && slotsToCheck == initialSlot)
			{
				column -= 1;
				slotsToCheck = gameBoard[initialRow, column];
				if (slotsToCheck == initialSlot)
					connected += 1;

				if (connected == 4)
				{
					return true;
				}
			}
			return false;
		}

		private static bool CheckRightHorizontal(int row, int column, int turns)
		{
			//check for right horizontal matches
			int connected = 1;
			int initialRow = row;
			int initialColumn = column;
			string initialSlot = gameBoard[initialRow, initialColumn];
			string slotsToCheck = gameBoard[row, column];


			while (column < 6 && slotsToCheck == initialSlot) // keep checks in bounds
			{												// and continue if match found
				column += 1;								// increase column index
				slotsToCheck = gameBoard[initialRow, column];  // moves slotsToCheck
				if (slotsToCheck == initialSlot)
					connected += 1;

				if (connected == 4)
				{
					return true;
				}
			}
			return false;
		}


		private static bool CheckDiagonal(int row, int column, int turns)
		{
			// start down to right, keep connected value, move up to left


			int connected = 1;
			int initialRow = row;
			int initialColumn = column;
			string initialSlot = gameBoard[initialRow, initialColumn];
			string slotsToCheck = gameBoard[row, column];

			// loop through spots down and to the right

			while (row < 5 && column < 6 && slotsToCheck == initialSlot)
			{
				column += 1;
				row += 1;
				slotsToCheck = gameBoard[row, column];
				if (slotsToCheck == initialSlot)
				{
					connected += 1;
				}

				if (connected == 4)
				{
					
					return true;
				}
			}

			//reset row and column to initial values
			row = initialRow;
			column = initialColumn;
			slotsToCheck = gameBoard[row, column];

			// loop through slots up and to the left

			while (row > 0 && column > 0 && slotsToCheck == initialSlot)
			{
				column -= 1;
				row -= 1;
				slotsToCheck = gameBoard[row, column];
				if (slotsToCheck == initialSlot)
				{
					connected += 1;
				}

				if (connected == 4)
				{

					return true;
				}

			}

			//reset row and column to initial values
			// reset connected

			connected = 1;
			row = initialRow;
			column = initialColumn;
			slotsToCheck = gameBoard[row, column];

			// loop through spots down and to the left

			while (row < 5 && column > 0 && slotsToCheck == initialSlot)
			{
				column -= 1;
				row += 1;
				slotsToCheck = gameBoard[row, column];
				if (slotsToCheck == initialSlot)
				{
					connected += 1;
				}

				if (connected == 4)
				{
					
					return true;
				}
			}


			//reset row and column to initial values
			row = initialRow;
			column = initialColumn;
			slotsToCheck = gameBoard[row, column];

			// loop through slots up and to the right

			while (row > 0 && column < 6 && slotsToCheck == initialSlot)
			{
				column += 1;
				row -= 1;
				slotsToCheck = gameBoard[row, column];
				if (slotsToCheck == initialSlot)
				{
					connected += 1;
				}

				if (connected == 4)
				{
					
					return true;
				}

			}
			return false;



		}




		public static bool CheckWinner(int row, int column, int turns)
		{
			if (CheckLeftHorizontal(row, column, turns) == true
				|| CheckRightHorizontal(row, column, turns) == true
				|| CheckVertical(row, column, turns) == true
				|| CheckDiagonal(row, column, turns) == true)
			{

				
				return true;

			}


			if (CheckTieGame() == true)
			{
				MessageBox.Show("Tie Game!");
				Form1.ClearBoard();
				return true;
			}
			return false;
		}


		public static bool CheckTieGame()
		{
			int usedSlots = 0;

			for (int r = 5; r > -1; r--)
			{
				for (int c = 6; c > -1; c--)
				{
					
					if (GameBoard.gameBoard[r, c] != "empty")
					{
						usedSlots += 1;
						if (usedSlots == 42)
						{

							
							return true;
						}

					}
					
				}
			}

			return false;
		}


	}
}
